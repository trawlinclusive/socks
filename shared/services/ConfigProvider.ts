import Provider from './Provider';
import getConfig from 'next/config';

const {publicRuntimeConfig, serverRuntimeConfig} = getConfig();

export type Config = {
  readonly NOODLES_URL: string;
};

const env = process.browser ? publicRuntimeConfig : serverRuntimeConfig;

const ConfigProvider = new Provider<Config, keyof Config>('ConfigProvider', env);

export default ConfigProvider;

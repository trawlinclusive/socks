interface Window {
  INITIAL_PROPS: object;
  INITIAL_STATE: object;
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__<R>(...funcs: Function[]): (...args: any[]) => R;
}

// enable import .md files
declare module '*.md' {
  const value: string;
  export default value;
}

// enable import .jpg files
declare module '*.jpg' {
  const value: string;
  export default value;
}

declare module '*.png' {
  const value: string;
  export default value;
}

declare namespace NodeJS {
  export interface Process {
    browser: boolean;
  }
}

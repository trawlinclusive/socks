#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

npm -f version $1

VERSION=$(cat ./package.json | jq -r '.version')
SHOULD_RELEASE=$1

npm test
npm run build

if [[ ! -z "$SHOULD_RELEASE" ]]
then
    docker build . -t cloud.canister.io:5000/akeem/socks:${VERSION} cloud.canister.io:5000/akeem/socks:latest
else
    docker build . -t cloud.canister.io:5000/akeem/socks:${VERSION}
fi


git commit package.json -m "Updated to version: ${VERSION}"

git push
git push --tags

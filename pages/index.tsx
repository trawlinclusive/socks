import React, {Component} from 'react';

class Index extends Component<any, any> {
  static getInitialProps = async () => {
    try {
      return {name: 'hello'};
    } catch(e) {
      throw new Error('Unable to fetch homepage content');
    }
  };

  state = {
    data: null,
  };

  render() {
    const {name} = this.props;
    const {data} = this.state;
    return (
        <div>
          {name}<br/>
          {data}
        </div>
    );
  }

  componentDidMount() {
    this.getExplore();
    

  }

  async getExplore() {
    this.setState({data: "data.name"});
  }
}

export default Index;
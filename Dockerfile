FROM node:10.13-alpine

WORKDIR /usr/src/app

COPY .next .next
COPY static static
COPY next.config.js .
COPY config config
COPY package.json .
RUN npm install --only=production

EXPOSE 3000
CMD ["npm", "run", "start", "--", "--port", "3000"]

const withTypescript = require('@zeit/next-typescript');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const env = require('./config');

module.exports = withTypescript({
  webpack(config, options) {
    // Do not run type checking twice:
    if (options.isServer) config.plugins.push(new ForkTsCheckerWebpackPlugin());
    return config;
  },
  publicRuntimeConfig: {
    ...env.global,
    ...env.client,
  },
  serverRuntimeConfig: {
    ...env.global,
    ...env.server,
  },
});
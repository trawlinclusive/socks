const path = require('path');
const fs = require('fs');

const env = process.env.NODE_ENV || 'development';

const CONFIG_FILE_PATH = path.resolve(__dirname, `${env}.js`);

try {
  fs.statSync(CONFIG_FILE_PATH);
}
catch (e) {
  console.log(`Config file for environment not found at ${CONFIG_FILE_PATH}`);
  process.exit(1);
}

module.exports = require(CONFIG_FILE_PATH);

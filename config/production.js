module.exports = {
  "global": {},
  "client": {
    "NOODLES_URL": process.env.API_URL
  },
  "server": {
    "NOODLES_URL": process.env.INTERNAL_API_URL
  }
};